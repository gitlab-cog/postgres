FROM ruby:2.3

# Setup bundle user and directory
RUN useradd bundle && \
	mkdir -p /home/bundle && \
	chown -R bundle /home/bundle

WORKDIR /home/bundle
COPY . /home/bundle/

USER bundle

RUN gem install bundler && bundle install --without development test --path .bundle
