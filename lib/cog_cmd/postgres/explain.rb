require "cog"
require "postgres/execute"

module CogCmd
  module Postgres
    class Explain < Cog::Command
      def run_command
        result = ::Postgres::Client.new.execute(query)
        response.content = response_json(*result)
      end

      private

      def query
        fail Cog::Error.new("No query passed!") if request.args.empty?

        "EXPLAIN #{request.args.join(' ')}"
      end

      def response_json(values, err = nil)
        {
          rows: values.flatten, # As results are always wrapped in an array
          error: err
        }.to_json
      end
    end
  end
end
