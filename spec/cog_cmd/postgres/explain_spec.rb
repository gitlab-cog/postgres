require "spec_helper"
require "cog_cmd/postgres/explain"

describe CogCmd::Postgres::Explain do
  let(:result) do
    [
      ["Aggregate  (cost=3.49..3.50 rows=1 width=8)",
       "  ->  Seq Scan on users  (cost=0.00..3.39 rows=39 width=0)"],
      nil
    ]
  end

  before do
    allow(STDIN).to receive(:tty?) { true }

    allow_any_instance_of(::Postgres::Client).to receive(:execute)
      .and_return(result)
  end

  it "executes a query on PG " do
    pg_select = CogCmd::Postgres::Explain.new

    with_environment("SELECT COUNT(*) FROM users;") do
      pg_select.run_command

      expect(pg_select.response.content).to eq(
        "{\"rows\":[\"Aggregate  (cost=3.49..3.50 rows=1 width=8)\",\"" \
        "  ->  Seq Scan on users  (cost=0.00..3.39 rows=39 width=0)\"]," \
        "\"error\":null}"
      )
    end
  end
end
